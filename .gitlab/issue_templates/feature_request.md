# Prerequisites

Please answer the following questions for yourself before submitting a feature
request.

- [ ] I checked the documentation and made sure this does not already exist
- [ ] I checked to make sure that this feature has not already been requested

# Feature Overview

Please describe what it is that you would like to see in this application

## Intended Behavior

Please describe how you would expect this feature to work (if possible)

## Context

Please describe why you want/need this feature and what other benefits
it could have for users

# Relevant Files

Please include any relevant log snippets, mockups, or files here.