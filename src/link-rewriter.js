// ==UserScript==
// @name         4chan Image URL Rewriter
// @namespace    http://tampermonkey.net/
// @version      1.2.0
// @description  Rewrites the flash file and full resolution image addresses on 4chan, and updates links whenever new posts are added.
// @author       Luna Lucadou
// @homepage     https://luna.lucadou.sh/
// @match        https://boards.4chan.org/*
// @match        https://boards.4channel.org/*
// @grant        none
// ==/UserScript==

(function() {
    'use strict';

    let updateLinks = function() {
        let count = 0;
        let selectors = [
            [ // Image thumbnail - 4chanX
                'div.file a.fileThumb[href^="https://i.4cdn.org/"]',
                /(https:\/\/i)\.4cdn\.org(\/\w+\/.+?\.\w+)/
            ],
            [ // Image thumbnail - 4chanX, if it were to start using is2
                'div.file a.fileThumb[href^="https://is2.4chan.org/"]',
                /(https:\/\/i)s2\.4chan\.org(\/\w+\/.+?\.\w+)/
            ],
            [ // Image thumbnail - Stock 4chan
                'div.file a.fileThumb[href^="//is2.4chan.org/"]',
                /(https:\/\/i)s2\.4chan\.org(\/\w+\/.+?\.\w+)/
            ],
            [ // Image thumbnail - Stock 4chan, if it were to go back to 4cdn
                'div.file a.fileThumb[href^="//i.4cdn.org/"]',
                /(https:\/\/i)\.4cdn\.org(\/\w+\/.+?\.\w+)/
            ],
            [ // Filename permalink - 4chanX
                'span.file-info a[href^="https://i.4cdn.org/"]',
                /(https:\/\/i)\.4cdn\.org(\/\w+\/.+?\.\w+)/
            ],
            [ // Filename permalink - 4chanX, if it were to start using is2
                'span.file-info a[href^="https://i.4cdn.org/"]',
                /(https:\/\/i)s2\.4chan\.org(\/\w+\/.+?\.\w+)/
            ],
            [ // Filename permalink - Stock 4chan
                'span.fileText a[href^="//i.4cdn.org/"]',
                /(https:\/\/i)s2\.4chan\.org(\/\w+\/.+?\.\w+)/
            ],
            [ // Filename permalink - Stock 4chan, if it were to start using is2 for Flash content
                'span.fileText a[href^="//is2.4chan.org/"]',
                /(https:\/\/i)\.4cdn\.org(\/\w+\/.+?\.\w+)/
            ]
        ];
		/* Rewrite file links */
        selectors.forEach(function(selector_pair) {
            let dom_matcher = selector_pair[0];
            let starting_regex = selector_pair[1];
            let replacing_regex = /$1.4co.be$2/;
            document.querySelectorAll(dom_matcher).forEach(function(element) {
                // Selector specifically excludes links that are already changed
                count++;
                let current_link = element.href;

                // Change domain
                let rewritten_link = current_link.replace(starting_regex, replacing_regex);
                // Remove starting and ending forward slashes
                rewritten_link = rewritten_link.substring(1, rewritten_link.length-1);
                // Change link in DOM
                element.href = rewritten_link;
            });
        });
        console.log(count + ' links changed.');
    }

    console.log('Initial page load link rewrite in progress...');
    updateLinks();

    // Setup observer for new posts
    let observer = new MutationObserver(function(mutations, observer) {
        // Fired when a mutation occurs (within 'div.board div.thread', aka when a post has been added or deleted)
        // https://stackoverflow.com/a/39332340
        console.log('Thread updated, rechecking links...');
        updateLinks();
    }).observe(document.querySelector('div.board div.thread'), {childList: true});
    // childList - Set to true if mutations to target’s children are to be observed.
    // https://dom.spec.whatwg.org/#interface-mutationobserver
})();

