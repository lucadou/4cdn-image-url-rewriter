# 4chan Image URL Rewriter

This userscript automatically rewrites 4chan image links (not the preview links,
just the URLs to the full-sized files on `i.4cdn.org/is2.4chan.org`)
with the equivalent link on `i.4co.be`, a site which is just a redirect for `i.4cdn.org`.

## Getting Started

### Prerequisites

* Any modern browser.
* Tampermonkey or another userscript extension.

### Installation (Tampermonkey)

* Open Tampermonkey Dashboard.
* Go to the Utilities tab.
* Inside the "Install from URL" box, paste the link
`https://gitlab.com/lucadou/4cdn-image-url-rewriter/-/raw/master/src/link-rewriter.js`
and click "Install".
* Click the "Install" or "Update" button that appears on the next page.

## FAQ

> Why was this created?

This script was created because a Twitch chat I am in blocks 4chan image links
(you can post threads just fine, just not links to the images), so we'd have to
split them up (like `i.4 cdn.org`), which gets annoying. One workaround was to
setup a redirect on a router's webserver (`cdn.org` to `i.4cdn.org`) and create
a DNS entry for `cdn.org` to point to the router, but this runs into problems
when not using the router for DNS and when trying to set this up on many cheap
consumer routers. So, a better solution was needed.

One of the users, Anony54, set up the domain `i.egb.is` as a CNAME so we would
not have to manually split and join links. However, having to type a different
domain was annoying, and the user diggleshut suggested writing a script to
automatically change the links on 4chan threads, so I did.

However, CloudFlare eventually broke the functionality `i.egb.is` relied on,
so I bought the domain `4co.be` and setup a simple S3 bucket to redirect
everything to `i.4cdn.org` and put it behind a CloudFront distribution.
Short of the 4chan admins explicitly banning referrals from this hostname,
it should be impervious to changes in implementation details.

Initially I tried using the `copy` event listener to detect when a link was
copied, but it turns out this falls on its face when not dealing with directly
copying text on the page, so I decided to update all of the links.
I also wrote it to detect new posts and automatically update them.
Since it does not update the image source URLs, no redownloading of images takes place.

If 4chan changes the structure of thread pages, this script will probably break.
If that happens, feel free to submit an issue.

## Versioning

We use Semantic Versioning, aka [SemVer](https://semver.org/spec/v2.0.0.html).

## Authors

* Luna Lucadou - Wrote the script.

## License

This project is licensed under the GNU Affero General Public License v3.0 - see the
[license](LICENSE.md) file for details.

## Acknowledgements

* Anony54 - Created the initial domain this script used.
* diggleshut - Had the original idea for this script.

