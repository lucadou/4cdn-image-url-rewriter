# Changelog

All notable changes to this project will be documented in this file.

## [1.2.0] - 2024-12-26

### Added

- Filename permalink rewriting

### Changed

- Rewrite domain switched from `egb.is` to `4co.be` (#2)

### Removed

- `egb.is` Flash embedder (#2)

## [1.1.0] - 2024-09-14

### Added

- GitLab issue and MR templates
- Support for the `is2.4chan.org` domain and stock 4chan (#1)

### Changed

- License to the AGPL 3
- Readme

## [1.0.5] - 2020-10-29

### Changed
- Downgraded flash links to http since the domain does not have a trusted certificate setup
- Updated readme with an explanation for why egb.is does not use HTTPS

## [1.0.4] - 2020-10-29

### Changed
- Fixed syntax error in regex

## [1.0.3] - 2020-10-29

### Changed
- SWF links on /f/ are now rewritten

## [1.0.2] - 2020-10-11

### Changed
- Rewrite domain changed from "egb.red" to "egb.is"

## [1.0.1] - 2020-04-23

### Added

- License added
- Readme added

### Changed

- Whitespace in script

## [1.0.0] - 2020-04-23

### Added

- Repo created
- Script created
